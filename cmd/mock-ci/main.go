package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"log"
)

type Response struct {
	Status string `json:"status"`
}

func ciMockReturn(w http.ResponseWriter, req *http.Request) {
	r := Response{
		Status: "running",
	}

	statusJson, err := json.Marshal(r)

	if err != nil {
		fmt.Println("Error: ", err)
	}

	w.Header().Set("Content-Type", "application/json")

	w.Write(statusJson)

	log.Printf("%s, Response sent successfully", req.RemoteAddr)
}

func main() {
	mux := http.NewServeMux()
	mux.HandleFunc("/", ciMockReturn)

	log.Printf("Starting HTTP server on the 4004 port")
	log.Fatal(http.ListenAndServe(":4004", mux))
}
