# GitLab Mock CI Service

This is a Go based implementation of [GitLab mock CI service](https://gitlab.com/gitlab-org/gitlab-mock-ci-service)

## Installation

Go version 1.22.2 and above is needed, no extra dependencies are required

The `main.go` file is inside the `cmd/mock-ci` folder. The directory structure of this project is based on the [Standard Go Layout](https://github.com/golang-standards/project-layout).

```bash
$ go run cmd/mock-ci/main.go
> 2024/05/14 16:06:44 Starting HTTP server on the 4004 port
```

## Setup integration

To setup the mock CI integration from GitLab version 17.0 (as of the time of this writing). ****This only applies to the GDK****

Inside `Settings -> Integrations -> Mock CI`

![Mock CI settings](doc/mock_ci.png)
